extends Node2D

var target = Vector2(0, 0)
var origin
var moving = false

var speed = 0
var acceleration_down = 3500
var max_speed_down = 2000
var acceleration_up = 100
var max_speed_up = 250
var max_speed_horizontal = 750

var state_smash = preload("fist_state_smash.gd").new(self)
var state_idle = preload("fist_state_idle.gd").new(self)
var state_raise = preload("fist_state_raise.gd").new(self)

var current_state

func _ready():
	var state = preload("state.gd").new(self)

	origin = Vector2(get_viewport_rect().size.width * 0.5, 807)

	current_state = state_idle

	set_process(true)
	set_process_input(true)

	update()


func _process(delta):

	var x = get_parent().get_local_mouse_pos().x

	trim_speed()

	current_state.update(delta)


func _input(event):

	if event.type == InputEvent.MOUSE_BUTTON:
		if not moving and event.pos.y > get_viewport_rect().size.height * 0.5:

			target = limit_input_to_screen(event.pos)
			smash_the_ground()


# TODO: Rename to better reflect what it does.
# Ensures the fist doesn't go outside the screen.
func limit_input_to_screen(pos):

	var new_pos = Vector2(pos.x, 1007)
	var texture_width = get_node("Sprite").get_texture().get_size().width
	var screen_size = get_viewport_rect().size.width

	var fist_right_edge = pos.x + texture_width * 0.5
	var fist_left_edge = pos.x - texture_width * 0.5

	if fist_right_edge > 720:
		new_pos.x = screen_size - texture_width * 0.5
	elif fist_left_edge < 0:
		new_pos.x = texture_width * 0.5

	return new_pos


func smash_the_ground():

	moving = true

	current_state = state_smash
	current_state.start()


func trim_speed():

	if speed > max_speed_down:
		speed = max_speed_down