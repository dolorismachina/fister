extends Node


var fist
var dir

func _init(obj):
	fist = obj


func start():
	print("State changed to smash.")
	dir = (fist.target - fist.get_pos()).normalized()

func update(delta):

	move_towards_target(fist.target, delta)

	if target_reached():
		fist.get_node("SamplePlayer2D").play("fist_smash")

		fist.current_state = fist.state_idle
		fist.current_state.start()


func move_towards_target(pos, delta):


	fist.speed += fist.acceleration_down * delta
	fist.translate(Vector2(dir.x * fist.speed * delta, dir.y * fist.speed * delta))


func target_reached():
	return fist.get_pos().y >= 1007
