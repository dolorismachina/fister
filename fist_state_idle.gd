extends Node

var fist

var timer = 0

func _init(obj):
	fist = obj

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func start():
	print("State changed to idle.")
	fist.speed = 0
	timer = 0


func update(delta):
	timer += delta
	if fist.get_pos().y >= 1007 and timer > 0.5:
		fist.current_state = fist.state_raise
		fist.current_state.start()

