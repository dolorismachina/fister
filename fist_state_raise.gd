extends Node

var fist

var timer = 0

func _init(obj):
	fist = obj
	print("RAISE")


func start():
	print("State changed to raise.")
	fist.target = fist.origin
	print(fist.target)


func update(delta):

	move_towards_target(fist.target, delta)
	rotate_towards_target(delta)

	if target_reached():
		fist.moving = false
		fist.current_state = fist.state_idle;
		fist.current_state.start()


func move_towards_target(pos, delta):

	var dir = (pos - fist.get_pos()).normalized()

	fist.speed += fist.acceleration_up * delta
	fist.translate(Vector2(dir.x * fist.speed * delta, dir.y * fist.speed * delta))


func target_reached():

	return fist.get_pos().y <= fist.target.y


func rotate_towards_target(delta):

	var angle = fist.get_rot()
	fist.rotate(angle * 0.5 * delta * -1)